FROM golang:1.24-alpine AS gcr-creds-helper
RUN go install github.com/GoogleCloudPlatform/docker-credential-gcr/v2@latest


FROM docker:git

ENV TERRAFORM_VERSION=1.11.0 \
    PATH=$PATH:/root/google-cloud-sdk/bin

RUN apk --update-cache add \
        binutils \
        curl \
        groff \
        bash \
        jq \
        docker-credential-ecr-login \
        gettext \
        python3 \
        which \
        unzip \
        aws-cli \
    && sed -i 's/ash/bash/g' /etc/passwd \
    && curl -sSL https://sdk.cloud.google.com | bash \
    && curl -LO https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip \
    && unzip terraform_${TERRAFORM_VERSION}_linux_amd64.zip -d /usr/local/bin \
    && rm terraform_${TERRAFORM_VERSION}_linux_amd64.zip \
    && apk --no-cache del \
        binutils \
        unzip \
    && rm -rf /var/cache/apk/* \
    && docker --version \
    && aws --version \
    && terraform --version \
    && gcloud --version \
    && gcloud components install beta

COPY --from=gcr-creds-helper /go/bin/docker-credential-gcr /usr/bin/docker-credential-gcr

CMD /bin/bash
